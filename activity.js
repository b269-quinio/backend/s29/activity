// Database

db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);

// Insert embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});


/*-------------------------------------------------------------------*/

//ACTIVITY

// Find useres withj letter s in their first name or di in their last name.
db.users.find({ $or: [
	{ firstName: {$regex: 's', $options: '$i'} }, 
	{ lastName: {$regex: 'd', $options: '$i'} }]},

	 {
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);



// Result of querying users who are from the HR department and their age is greater than or equal to 70.
db.users.find({ $and: [
	{ department: 'HR' },
	{ age: {$gte: 70}}
	]
});


// Result of querying users with the letter e in their first name and has an aage of less than or equal to 30.

db.users.find({ $and: [
		{ firstName: {$regex: 'e'} },
		{ age: {$lte: 30} }
	]
});